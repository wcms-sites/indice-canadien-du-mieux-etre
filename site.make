core = 7.x
api = 2

; transliteration
; Exists in main makefile. @todo Explain why this version is needed or remove it.
projects[transliteration][type] = "module"
projects[transliteration][download][type] = "git"
projects[transliteration][download][url] = "https://git.uwaterloo.ca/drupal-org/transliteration.git"
projects[transliteration][download][tag] = "7.x-3.1"
projects[transliteration][subdir] = ""

; uw_cta_styling
projects[uw_cta_styling][type] = "module"
projects[uw_cta_styling][download][type] = "git"
projects[uw_cta_styling][download][url] = "https://git.uwaterloo.ca/wcms/uw_cta_styling.git"
projects[uw_cta_styling][download][tag] = "7.x-1.0"
projects[uw_cta_styling][subdir] = ""

; uw_gallery_fr
projects[uw_gallery_fr][type] = "module"
projects[uw_gallery_fr][download][type] = "git"
projects[uw_gallery_fr][download][url] = "https://git.uwaterloo.ca/wcms/uw_gallery_fr.git"
projects[uw_gallery_fr][download][tag] = "7.x-1.0"
projects[uw_gallery_fr][subdir] = ""
